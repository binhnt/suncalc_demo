var sunInfoOverlay;
var mapCenter = new google.maps.LatLng(21.036, 105.842);
$(function () {
	initMap(mapCenter);

	var sunInfo = getSunInfo(new Date(), mapCenter);
	for(var key in sunInfo) {
	    var datetime = new Date(sunInfo[key]);
	    var hours = formatDateTimeNumber(datetime.getHours());
	    var minutes = formatDateTimeNumber(datetime.getMinutes());
	    var datetimeFormat = hours + ":" + minutes;
	    $('#suninfo-table').append('<tr><td><span class="suntime">' + datetimeFormat + '</span></td><td class="suntime-label">' + key + '</td></tr>');

	    if ((key == 'dawn') || (key == 'sunrise') || (key == 'solarNoon') || (key == 'sunset') || (key == 'dusk')) {
	    	$('#suninfo-table-short').append('<tr><td><span class="suntime">' + datetimeFormat + '</span></td><td class="suntime-label">' + key + '</td></tr>');
	    }
	}

	$('.toogle-info').on('click', function() {
		$('#suninfo-panel').toggleClass('short');
	});

	$('#time-slider').slider({
		value: 0,
		formatter: function(value) {
			var hour =  formatDateTimeNumber(parseInt(value / 60));
			var minute = formatDateTimeNumber(value % 60);
			return hour + ':' + minute;
		}
	});

	$('#time-slider').on('slide', function(event) {
		var value = event.value;
		if (typeof value == 'undefined')
			value = 0;
		var hour =  formatDateTimeNumber(parseInt(value / 60));
		var minute = formatDateTimeNumber(value % 60);
		$('#time-display span').text(hour + ':' + minute);
		var current = new Date();
		current.setHours(hour);
		current.setMinutes(minute);
		sunInfoOverlay.update(mapCenter, current);
	});


});

var formatDateTimeNumber = function(dateNumber) {
	return dateNumber < 10 ? ('0' + dateNumber) : dateNumber;
}

var initMap = function(mapCenter) {
	var map;
	function initialize(){
		var mapProp = {
			center: mapCenter, //hanoi
			zoom: 7,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("mapcanvas"), mapProp);

		var marker = new google.maps.Marker({
			position: map.getCenter(),
			draggable: false 
		});
		marker.setMap(map);

		sunInfoOverlay = new SuncalcOverlay(map, map.getCenter(), new Date());
	}
	google.maps.event.addDomListener(window, 'load', initialize);
}


var getSunInfo = function (date, position) {
	return SunCalc.getTimes(date, position.lat(), position.lng());
}